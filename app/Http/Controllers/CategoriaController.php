<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    //accion: UN METODO CONTROLADOR, CONTIENEN EL CODIGO A EJECUTAR
    //nombre: PUEDE SER CUALQUIERA
    //RECOMENDADO EL NOMBRE EN MINUSCULA   

    public function index(){
        //SELECCIONAR LAS CATEGORIAS EXISTENTES
        $categorias = Categoria::paginate(5);      
        //enviar la coleccion de categorias a una vista
        //y las vamos a mostrar alli
        return view("categorias.index")->with("categorias", $categorias);    
    }


    //MOSTRAR EL FORMULARIO DE CREAR CATEGORIA
    public function create(){
        return view("categorias.new");
    }



    public function store(Request $r){
        //Validacion
        //1. Establecer reglas validacion campos
        $reglas = [
            "txtCategoria" => ["required","alpha"]
        ];
        $mensajes = [
            "required" => "Campo Obligatorio",
            "alpha" => "Solo letras"
        ];
        //2.Crear objeto validador
        $validador = Validator::make($r->all(), $reglas, $mensajes);
        //3.Validar metodo = Fails
         //retorna true si al validar no cumple con las condiciones
        //retorna falso si es los datos son correctos
        if ($validador->fails()) {
            //Codigo para cuando al validar no cumple las condiciones
            return redirect("categorias/create")->withErrors($validador);

        }else{
            //Codigo si la validacion es correcta
        }
        //Crear nueva categoria
        $categoria = new Categoria();
        //Asignar nombre
        $categoria -> name = $r->input("txtCategoria");
        //guardar la nueva categoria
        $categoria -> save();
        
        return redirect('categorias/create')->with("mensaje", "Categoria Guardada");
    }

    public function edit($category_id){
        //seleccionar la categoria a editar
        $categoria = Categoria::find($category_id);

        //mostrar la vista de actualizar categoria
        //llevando dentro la categoria
        return view("categorias.edit")->with("categoria", $categoria);
    }
    public function update($category_id){
        //Validacion
        //1. Establecer reglas validacion campos
        $reglas = [
            "txtCategoria" => ["required","alpha"]
        ];
        $mensajes = [
            "required" => "Campo Obligatorio",
            "alpha" => "Solo letras"
        ];
        //2.Crear objeto validador
        $validador = Validator::make($category_id->all(), $reglas, $mensajes);
        //3.Validar metodo = Fails
         //retorna true si al validar no cumple con las condiciones
        //retorna falso si es los datos son correctos
        if ($validador->fails()) {
            //Codigo para cuando al validar no cumple las condiciones
            return redirect("categorias/create")->withErrors($validador);

        }else{
            //Codigo si la validacion es correcta
        }
        //Seleccionar la categoria a editar
        $categoria = Categoria::find($category_id);
        //editar atributos
        $categoria->name = $_POST["txtCategoria"];
        //guardar cambios
        $categoria->save();
        //retoranar al formulario edit
        return redirect("categorias/edit/$category_id")->with ("mensaje", "Categoria editada");

    }


}

